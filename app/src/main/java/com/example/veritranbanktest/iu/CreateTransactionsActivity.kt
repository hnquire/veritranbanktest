package com.example.veritranbanktest.iu

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.veritranbanktest.R
import com.example.veritranbanktest.enum.Constant
import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VAccount
import com.example.veritranbanktest.model.VTransaction
import com.example.veritranbanktest.network.TransactionsAPI
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_create_transactions.*

class CreateTransactionsActivity : AppCompatActivity() {

    private var mTransaction = VTransaction()
    private var mAccount = VAccount()
    lateinit var option: Spinner
    lateinit var result: String
    var transactionTypeCode: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_transactions)

        mAccount = UserData.accountSelected

        option = findViewById(R.id.sp_transactionType)
        result = "0"

        tv_account_name.text = UserData.accountSelected.accountNickname
        tv_amount.text = ("U" + "$" + "D " + UserData.accountSelected.balance.toString())

        val options = Constant.TransactionType.values().map { it.name }
        option.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, options)

        // WOD Selector Spinner
        option.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                result = "Please select an option"
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                result = options[position]
                transactionTypeCode = (position)

                if (transactionTypeCode == 0 || transactionTypeCode == 1) {
                    tv_account_to.visibility = (View.GONE)
                    et_account_to.visibility = (View.GONE)
                } else {
                    tv_account_to.visibility = (View.VISIBLE)
                    et_account_to.visibility = (View.VISIBLE)
                }
            }
        }

        bn_make_transaction.setOnClickListener {
            val intent = Intent(it.context, AccountDetailActivity::class.java)
            if (transactionTypeCode != 0 && et_transaction_amount.text.toString().toDouble() > mAccount.balance) {
//                Toast.makeText(this, "You cannot withdraw more than: ${mAccount.balance}", Toast.LENGTH_LONG).show()
                Toasty.error(
                    this,
                    "Overdraft! You cannot withdraw more than: ${mAccount.balance}",
                    Toast.LENGTH_SHORT,
                    true
                ).show()
            } else {
                setworkoutsList()
                intent.putExtra("Account", mAccount)
                it.context.startActivity(intent)
                Toasty.success(this, "Success!", Toast.LENGTH_SHORT, true).show()
            }

        }
    }

    private fun setworkoutsList() {

        mTransaction.transactionType = transactionTypeCode
        if (mTransaction.transactionType == Constant.TransactionType.WITHDRAWAL.transactionTypeNumber && mTransaction.transactionType == Constant.TransactionType.TRANSFERENCE.transactionTypeNumber) {
            mTransaction.balance = mTransaction.balance - mTransaction.transactionAmount
        }else {
            mTransaction.balance = mTransaction.balance + mTransaction.transactionAmount
        }
        mTransaction.transactionFrom = mAccount.accountOwner
        mTransaction.accountNumber = mAccount.accountNumber
        mTransaction.transactionAmount = et_transaction_amount.text.toString().toDouble()
        postTransactionList()
    }

    private fun postTransactionList() {
        TransactionsAPI().postTransactionList(mTransaction).subscribe(
            { transactionList ->
                this.mAccount.transactionsList = transactionList
            },
            {}
        )
    }
}
