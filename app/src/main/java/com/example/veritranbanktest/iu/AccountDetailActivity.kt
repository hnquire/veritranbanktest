package com.example.veritranbanktest.iu

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.veritranbanktest.R
import com.example.veritranbanktest.adapter.TransactionAdapter
import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VAccount
import com.example.veritranbanktest.model.VTransaction
import com.example.veritranbanktest.network.TransactionsAPI
import kotlinx.android.synthetic.main.activity_account_detail.*

class AccountDetailActivity : AppCompatActivity() {

    private var transferencesList = mutableListOf<VTransaction>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_detail)

        tv_account_name.text = UserData.accountSelected.accountNickname
        tv_amount.text = ("U"+"$"+"D " +UserData.accountSelected.balance.toString())

        val mAccount: VAccount = intent.getSerializableExtra("Account") as VAccount
        print(mAccount)
        if (mAccount.transactionsList?.size != 0) {
            transferencesList = mAccount.transactionsList!!
            setAdapterData()
        } else {
            getTransferences()
        }
    }


    private fun getTransferences() {
        TransactionsAPI().getTransaction().subscribe(
            { transferenceList ->
                this.transferencesList = transferenceList
                setAdapterData()
            },
            {}
        )
    }

    private fun setAdapterData() {
        rv_transactions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_transactions.adapter = TransactionAdapter(transferencesList)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()

        if (id == R.id.action_make_transaction) {
            val intent = Intent(this, CreateTransactionsActivity::class.java)
            startActivity(intent)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
