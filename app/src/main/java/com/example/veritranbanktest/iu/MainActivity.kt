package com.example.veritranbanktest.iu

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.veritranbanktest.R
import com.example.veritranbanktest.adapter.AccountAdapter
import com.example.veritranbanktest.network.AccountAPI
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAccounts()
    }

    private fun getAccounts() {
        AccountAPI().getAccounts().subscribe(
            {
                setAdapterData()
            },
            {}
        )
    }

    private fun setAdapterData() {
        rv_accounts.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_accounts.adapter = AccountAdapter()
    }
}
