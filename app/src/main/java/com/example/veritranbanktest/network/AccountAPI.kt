package com.example.veritranbanktest.network

import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VAccount
import io.reactivex.Completable

class AccountAPI {


    fun getAccounts(): Completable {
        return Completable.create { emitter ->
            TransactionsAPI().getTransaction()
            val accountList = createMockupAccounts()

            UserData.accountList = accountList
            UserData.accountSelected = accountList[0]

            emitter.onComplete()
        }

    }

    fun createMockupAccounts(): MutableList<VAccount> {
        val accountsList = mutableListOf<VAccount>()
        val account = VAccount()
        account.id = 1
        account.accountType = 0
        account.accountNickname = "Cuenta Corriente"
        account.accountOwner = "francisco"
        account.accountNumber = "38-123456789-01"
        account.balance = 100.0
        account.transactionsList = mutableListOf()

        accountsList.add(account)
        return accountsList
    }
}