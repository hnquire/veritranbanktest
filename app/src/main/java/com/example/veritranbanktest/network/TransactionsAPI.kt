package com.example.veritranbanktest.network

import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VAccount
import com.example.veritranbanktest.model.VTransaction
import io.reactivex.Single

class TransactionsAPI {

    fun getTransaction(): Single<MutableList<VTransaction>> {
        return Single.create<MutableList<VTransaction>> { emitter ->
            emitter.onSuccess(createMockupTransactions())
        }
    }

    fun postTransactionList(transaction: VTransaction): Single<MutableList<VTransaction>> {
        return Single.create<MutableList<VTransaction>> { emitter ->
            emitter.onSuccess(postTransaction(transaction))
        }
    }

    fun createMockupTransactions(): MutableList<VTransaction> {
        val transactionsList = mutableListOf<VTransaction>()
        val transaction = VTransaction()
        transaction.id = 1
        transaction.transactionType = 0
        transaction.transactionFrom = "francisco"
        transaction.accountNumber = "38-123456789-01"
        transaction.balance = 100.0
        transaction.transactionAmount = 100.0

        transactionsList.add(transaction)

        UserData.accountSelected.transactionsList = transactionsList

        return transactionsList
    }

    fun postTransaction(transaction: VTransaction): MutableList<VTransaction> {
        val mAccount = UserData.accountSelected
        val mTransaction = VTransaction()
        var mTransactionList: MutableList<VTransaction> = mutableListOf()

        mTransaction.id = transaction.id
        mTransaction.transactionType = transaction.transactionType
        mTransaction.transactionFrom = transaction.transactionFrom
        mTransaction.accountNumber = transaction.accountNumber
        if (transaction.transactionType == 0){
            mTransaction.transactionAmount = transaction.transactionAmount
            mTransaction.balance = mAccount.balance + transaction.transactionAmount
            mAccount.balance = mAccount.balance + mTransaction.transactionAmount
            mAccount.transactionsList!!.add(mTransaction)
        }else{
            if (transaction.transactionType == 1 || transaction.transactionType == 2 ){
                mTransaction.transactionAmount = transaction.transactionAmount
                mTransaction.balance = mAccount.balance - transaction.transactionAmount
                mAccount.balance = mAccount.balance - mTransaction.transactionAmount
                mAccount.transactionsList!!.add(mTransaction)
            }else{

            }
        }

        UserData.accountSelected.transactionsList = mAccount.transactionsList
        mTransactionList = mAccount.transactionsList!!

        return mTransactionList
    }
}