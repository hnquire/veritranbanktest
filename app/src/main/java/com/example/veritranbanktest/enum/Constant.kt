package com.example.veritranbanktest.enum

class Constant {

    enum class AccountType private constructor(val accountTypeNumber: Int) {
        CUENTA_CORRIENTE(0),
        CAJA_AHORRO_PESOS(1),
        CAJA_AHORRO_DOLAR(2),
    }

    enum class  TransactionType private constructor(val transactionTypeNumber: Int) {
        DEPOSIT(0),
        WITHDRAWAL (1),
        TRANSFERENCE(2),
    }
}