package com.example.veritranbanktest.adapter

import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.veritranbanktest.R
import com.example.veritranbanktest.enum.Constant
import com.example.veritranbanktest.model.VTransaction
import kotlinx.android.synthetic.main.transactions_card.view.*
import kotlin.math.absoluteValue

class TransactionAdapter(val transferencesList: MutableList<VTransaction>) : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.transactions_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return transferencesList.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {

        val fmt = SimpleDateFormat("yyyy/MM/dd")
        val dollar = ("U" + "$" + "D ")
        val transTypePosition = transferencesList[position].transactionType
        val transTypeName = Constant.TransactionType.values()[transTypePosition].name

        holder.transaction_view.tv_transaction_detail.text = transferencesList[position].transactionFrom
        holder.transaction_view.tv_transaction_date.text = fmt.format(transferencesList[position].transactionsDate).toString()
        holder.transaction_view.tv_total_amount.text = (dollar + transferencesList[position].balance.toString())
        holder.transaction_view.tv_transaction_type.text = transTypeName
        if (transferencesList[position].transactionType == 0){
            holder.transaction_view.tv_amount_transferred.text = (dollar + transferencesList[position].transactionAmount.toString())
        }else{
            holder.transaction_view.tv_amount_transferred.text = ("- " + dollar + transferencesList[position].transactionAmount.toString())
        }

        holder.transaction_view.setOnClickListener {
            Toast.makeText(it.context, "Transaction Selected", Toast.LENGTH_LONG).show()
        }
    }

    class TransactionViewHolder(val transaction_view: View) : RecyclerView.ViewHolder(transaction_view)
}