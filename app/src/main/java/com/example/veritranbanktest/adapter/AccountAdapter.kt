package com.example.veritranbanktest.adapter

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.veritranbanktest.R
import com.example.veritranbanktest.iu.AccountDetailActivity
import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VAccount
import kotlinx.android.synthetic.main.accounts_card.view.*

class AccountAdapter() : RecyclerView.Adapter<AccountAdapter.AccountViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        return AccountViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.accounts_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return UserData.accountList.size
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {

        holder.account_view.tv_account_name.text = UserData.accountList[position].accountNickname
        holder.account_view.tv_amount.text = ("U"+"$"+"D" + UserData.accountList[position].balance.toString())

        holder.account_view.setOnClickListener {
            val intent = Intent(it.context, AccountDetailActivity::class.java)
            intent.putExtra("Account", UserData.accountList[position])
            it.context.startActivity(intent)
        }
    }

    class AccountViewHolder(val account_view: View) : RecyclerView.ViewHolder(account_view)
}