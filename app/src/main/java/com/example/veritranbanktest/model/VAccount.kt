package com.example.veritranbanktest.model

import java.io.Serializable

class VAccount(
    var accountType: Int = 1,
    var accountNickname: String = "",
    var accountOwner: String = "",
    var id: Int = 0,
    var accountNumber: String = "",
    var balance: Double = 0.0,
    var transactionsList: MutableList<VTransaction>? = null


) : Serializable {
    fun map(data: Map<String, Any>) {
        accountType = data["accountType"] as Int
        accountNickname = data["accountNickname"] as String
        accountOwner = data["accountOwner"] as String
        id = data["id"] as Int
        accountNumber = data["accountNumber"] as String
        balance = data["balance"] as Double
        transactionsList = data["transactionsList"] as MutableList<VTransaction>
    }
}