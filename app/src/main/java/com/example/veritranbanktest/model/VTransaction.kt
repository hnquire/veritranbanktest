package com.example.veritranbanktest.model

import com.example.veritranbanktest.enum.Constant
import java.io.Serializable
import java.util.*


class VTransaction(
    var id: Int = 0,
    var transactionType: Int = 0,
    var transactionFrom: String = "",
    var transactionsDate: Date? = Date(),
    var accountNumber: String = "",
    var balance: Double = 0.0,
    var transactionAmount: Double = 0.0

) : Serializable {
    fun map(data: Map<String, Any>) {
        id = data["id"] as Int
        transactionType = data["transactionType"] as Int
        transactionFrom = data["transactionFrom"] as String
        transactionsDate = data["transactionsDate"] as Date?
        accountNumber = data["accountNumber"] as String
        balance = data["balance"] as Double
        transactionAmount = data["amountTransfered"] as Double
    }
}