package com.example.veritranbanktest

import com.example.veritranbanktest.enum.Constant
import com.example.veritranbanktest.model.UserData
import com.example.veritranbanktest.model.VTransaction
import com.example.veritranbanktest.network.AccountAPI
import com.example.veritranbanktest.network.TransactionsAPI
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test

class NegativeWithdrawalUT {
    @Test
    fun overdraftWithdrawal() {
        AccountAPI().getAccounts().subscribe(
            {
                makeWithdrawal()
            },
            {}
        )
    }

    private fun makeWithdrawal() {
        val mTransaction = VTransaction()
        mTransaction.transactionType = Constant.TransactionType.WITHDRAWAL.transactionTypeNumber
        mTransaction.transactionFrom = UserData.accountSelected.accountOwner
        mTransaction.accountNumber = UserData.accountSelected.accountNumber

        val withdrawalAmount= -10.0
        mTransaction.transactionAmount = withdrawalAmount
        postTransactionList(mTransaction)
    }

    private fun postTransactionList(transaction: VTransaction) {
        TransactionsAPI().postTransactionList(transaction).subscribe(
            { transactionList ->
                Assert.assertTrue(UserData.accountSelected.transactionsList?.size == 1)
                Assert.assertFalse(UserData.accountSelected.balance == 90.0)
            },
            {}
        )
    }
}
